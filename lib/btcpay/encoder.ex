defmodule BTCPay.Encoder do
  @moduledoc """
  Encodes native Elixir structs into BTCPay API request body strings.
  """

  def encode(struct) do
    struct
    |> Map.from_struct()
    |> Enum.reject(fn {_, v} -> is_nil(v) end)
    |> Map.new()
    |> Jason.encode()
  end
end

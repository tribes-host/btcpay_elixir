defmodule BTCPay.PaymentRequest do
  @moduledoc """
  https://docs.btcpayserver.org/API/Greenfield/v1/#tag/Payment-Requests
  """
  alias BTCPay.Encoder
  alias BTCPay.Decoder
  alias BTCPay.PaymentRequest

  @derive Jason.Encoder
  defstruct id: nil,
            status: nil,
            title: nil,
            description: nil,
            email: nil,
            amount: nil,
            currency: nil,
            expiryDate: nil,
            allowCustomPaymentAmounts: false,
            embeddedCSS: nil,
            customCSSLink: nil,
            created: nil,
            archived: nil

  def all(store_id) do
    endpoint = "/api/v1/stores/#{store_id}/payment-requests"

    with {:ok, %{status_code: 200, body: body}} <- http_client().get(endpoint) do
      {:ok, Decoder.decode(PaymentRequest, body)}
    else
      error -> {:error, error}
    end
  end

  def get(%PaymentRequest{id: id}, store_id), do: get(id, store_id)

  def get(id, store_id) do
    endpoint = "/api/v1/stores/#{store_id}/payment-requests/#{id}"

    with {:ok, %{status_code: 200, body: body}} <- http_client().get(endpoint) do
      {:ok, Decoder.decode(PaymentRequest, body)}
    else
      error -> {:error, error}
    end
  end

  def create(%PaymentRequest{} = payment_request, store_id) do
    endpoint = "/api/v1/stores/#{store_id}/payment-requests"

    with {:ok, data} <- Encoder.encode(payment_request),
         {:ok, %{status_code: 200, body: body}} <- http_client().post(endpoint, data) do
      {:ok, Decoder.decode(PaymentRequest, body)}
    else
      error -> {:error, error}
    end
  end

  def pay_url(%PaymentRequest{id: id}) do
    "https://#{Application.get_env(:btcpay, :host)}/payment-requests/#{id}/pay"
  end

  defp http_client(), do: Application.get_env(:btcpay, :http_client, BTCPay.Client)
end
